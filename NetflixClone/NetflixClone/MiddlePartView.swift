//
//  MiddlePartView.swift
//  NetflixClone
//
//  Created by Robertino Dimoski on 29.4.23.
//

import SwiftUI

struct MiddlePartView: View {
    var body: some View {
        VStack(spacing: 15) {
            HStack(spacing: 15) {
                ForEach(datas) {
                    i in
                    VStack(alignment: .leading, spacing: 0) {
                        Image(i.image)
                        HStack{
                            Spacer()
                            Button(action: {}){
                                Image("play").renderingMode(.original).resizable().frame(width: 25,height: 25)
                            }.padding(.top, -12)
                        }
                        VStack(alignment: .leading) {
                            Text(i.name)
                            Text(i.epname).foregroundColor(.gray)
                            ZStack{
                                Capsule().fill(Color(.gray))
                                HStack{
                                    Capsule().fill(Color("Color1")).frame(width: i.percentage)
                                    Spacer()
                                }
                            }.frame(height: 5)
                        }.padding(.horizontal, 10)
                            .padding(.bottom, 10)
                    }.background(Color("Color"))
                        .padding(.bottom)
                }
            }
        }
    }
}

struct MiddlePartView_Previews: PreviewProvider {
    static var previews: some View {
        MiddlePartView()
    }
}

