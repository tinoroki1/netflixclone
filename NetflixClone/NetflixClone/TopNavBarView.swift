//
//  HomeView.swift
//  NetflixClone
//
//  Created by Robertino Dimoski on 29.4.23.
//

import SwiftUI

struct TopNavBarView: View {
    @State var txt = ""
    @State var show = false
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            HStack(spacing: 10) {
                Button(action: {
                    
                }) {
                    Image("menu").renderingMode(.original)
                }
                Image("logo")
                Spacer()
                Button(action: {}){
                    Image("bell").renderingMode(.original)
                }
            }
            HStack(spacing: 15) {
               Image(systemName: "magnifyingglass").font(.body)
               TextField("Search For Movies,Shows", text: $txt)
           }.padding()
           .background(Color("Color"))
        }
        HeroSliderView().background(Color("Color")).padding()
    }
}

struct TopNavBarView_Previews: PreviewProvider {
    static var previews: some View {
        TopNavBarView()
    }
}
