//
//  BottomPartView.swift
//  NetflixClone
//
//  Created by Robertino Dimoski on 29.4.23.
//

import SwiftUI

struct BottomPartView: View {
    var body: some View {
        VStack(alignment: .leading,spacing: 15) {
            Text("Explore by genres")
            HStack{
                Button(action:{}){
                    Text("Action").padding()
                }.foregroundColor(.white)
                    .background(Color("Color1"))
                    .cornerRadius(10)
                Spacer(minLength: 10)
                Button(action:{}) {
                    Text("Advanture").padding()
                }.foregroundColor(.white).background(Color("Color1")).cornerRadius(10)
                Spacer(minLength: 10)
                Button(action: {}) {
                    Text("Comedy").padding()
                }.foregroundColor(.white).background(Color("Color1")).cornerRadius(10)
            }
            Text("Trending")
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 15) {
                    ForEach(bottom,id: \.self) {
                        i in Image(i)
                    }
                }
            }
        }
    }
}

struct BottomPartView_Previews: PreviewProvider {
    static var previews: some View {
        BottomPartView()
    }
}
