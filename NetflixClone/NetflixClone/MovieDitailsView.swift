//
//  MovieDitailsView.swift
//  NetflixClone
//
//  Created by Robertino Dimoski on 29.4.23.
//

import SwiftUI


struct type : Identifiable {
    
    var id : Int
    var name : String
    var epname : String
    var image : String
    var percentage : CGFloat
}

var datas = [
    type(id: 0, name: "Ghoul", epname: "Ep - S1",image: "m1",percentage : 45),
    type(id: 1, name: "13 Reasons Why", epname: "Ep - S2",image: "m2",percentage: 90)
]

var bottom = ["b1","b2"]

struct episodetype : Identifiable {
    
    var id : Int
    var name : String
    var time : String
    var image : String
    var desc : String
}

var episodes = [episodetype(id: 0, name: "The 'H' Word", time: "51 min", image: "d1", desc: "Matt Murdock, Jessica Jones, Luke Cage and Danny Rand investigate criminals and fight injustice, unaware their paths are about to cross."),
episodetype(id: 1, name: "Mean RIght Hook", time: "44 min", image: "d2", desc: "As a new conspiracy takes shape, Matt finds old habits are hard to break, Jessica gets in over her head, Luke tracks a lead, and Danny meets his match.")]


struct MovieDitailsView: View {
    @Binding var show: Bool
    var body: some View {
        VStack(spacing: 15){
            HStack(spacing: 15){
                Button(action: {self.show.toggle()}) {
                    Image("back").renderingMode(.original)
                }
                Spacer()
                Button(action: {}) {
                    Image("share").renderingMode(.original)
                }
                Button(action: {}) {
                    Image("info").renderingMode(.original)
                }
            }.padding()
            ZStack{
                Image("detail").resizable()
                VStack(alignment: .leading,spacing: 12){
                    Spacer()
                    Text("TV SERIES . ACTION")
                    Text("Marvel's The Defenders").font(.title)
                    HStack(spacing: 10){
                        Text("98% Match").foregroundColor(.green)
                        Text("2019")
                        Image("hd")
                        Spacer()
                    }
                }.padding()
            }.frame(height: UIScreen.main.bounds.height / 2)
            .navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .leading, spacing: 15){
                    HStack{
                        Button(action: {}) {
                            HStack(spacing: 10){
                                Image(systemName: "play.fill")
                                Text("Play")
                            }.padding()
                        }.foregroundColor(.white)
                        .background(Color("Color1"))
                        .cornerRadius(10)
                        Button(action: {}) {
                            HStack(spacing: 10){
                                Image(systemName: "plus")
                                Text("ADD TO LIST")
                            }.padding()
                        }.foregroundColor(.white)
                        .background(Color("Color"))
                        .cornerRadius(10)
                        Spacer()
                    }
                    
                    HStack{
                        
                        VStack(alignment: .leading, spacing: 10){
                            
                            Text("Episodes")
                            Text("Here are the episodes season 1").foregroundColor(.gray)
                        }
                        
                        Spacer()
                        
                        Button(action: {
                            
                        }) {
                            
                            HStack(spacing: 10){
                                
                                Image(systemName: "chevron.down")
                                Text("Season 1")
                                
                            }.padding()
                            
                        }.foregroundColor(.white)
                        .background(Color("Color"))
                        .cornerRadius(10)
                        
                    }.padding(.top, 15)
                    
                    Divider().padding(.vertical, 12)
                    
                    ForEach(episodes){i in
                        
                        VStack(alignment: .leading,spacing: 15){
                            
                            HStack{
                                
                                Image(i.image)
                                
                                VStack(alignment: .leading, spacing: 10){
                                    
                                    Text(i.name)
                                    Text(i.time)
                                    
                                }
                                
                                Spacer()
                                
                                
                                Button(action: {
                                    
                                }) {
                                    
                                    Image("download").renderingMode(.original)
                                }
                                
                            }
                            
                            Text(i.desc)
                        }
                        
                    }.padding(.bottom, 15)
                }
                
            }.padding(.top, 15)
            .padding(.horizontal, 15)
            
            
        }.edgesIgnoringSafeArea(.bottom)    }
}

struct MovieDitailsView_Previews: PreviewProvider {
    static var previews: some View {
        MovieDitailsView(show: .constant(true))
    }
}
