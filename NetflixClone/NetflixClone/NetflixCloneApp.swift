//
//  NetflixCloneApp.swift
//  NetflixClone
//
//  Created by Robertino Dimoski on 28.4.23.
//

import SwiftUI

@main
struct NetflixCloneApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
