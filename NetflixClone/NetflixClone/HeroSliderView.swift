//
//  HeroSliderView.swift
//  NetflixClone
//
//  Created by Robertino Dimoski on 29.4.23.
//

import SwiftUI

struct HeroSliderView: View {
    
    @State var txt = ""
    @State var show = false
    
    var body: some View {
        ScrollView(.vertical,showsIndicators: false) {
            VStack(alignment: .leading,spacing: 15) {
                Text("Netflix Originals").font(.title)
                ZStack{
                    NavigationLink(
                        destination: MovieDitailsView(show: $show),
                        isActive: $show) {
                            Text("")
                        }
                    
                }
            }
        }
    }
}

struct HeroSliderView_Previews: PreviewProvider {
    static var previews: some View {
        HeroSliderView()
    }
}
